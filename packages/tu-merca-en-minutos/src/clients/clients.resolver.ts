import { Resolver } from '@nestjs/graphql';
import { Client } from './ClientsSchema/clients.schema';
import { ClientsService } from './clients.service';
import { Query } from '@nestjs/graphql';

@Resolver((of) => Client)
export class ClientsResolver {
  constructor(private readonly clientsService: ClientsService) {}

  @Query((returns) => [Client])
  getAllClients() {
    return this.clientsService.findAll();
  }
}
