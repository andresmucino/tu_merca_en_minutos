import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientsEntity } from './entities/client.entity';
import { ClientsService } from './clients.service';
import { ClientsResolver } from './clients.resolver';

@Module({
  imports: [TypeOrmModule.forFeature([ClientsEntity])],
  controllers: [],
  providers: [ClientsService, ClientsResolver],
})
export class ClientsModule {}
