import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: "clients"})
export class ClientsEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  name: string
}