import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { ClientsEntity } from './entities/client.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class ClientsService {
  constructor(
    @InjectRepository(ClientsEntity)
    public readonly clientsRepository: Repository<ClientsEntity>,
  ) {}

  async findAll(): Promise<ClientsEntity[]> {
    let clients = await this.clientsRepository.find();

    return clients;
  }

  async findCLientById(id: number): Promise<ClientsEntity> {
    let client = await this.clientsRepository.findOne({ where: { id: id } });

    return client;
  }
}
