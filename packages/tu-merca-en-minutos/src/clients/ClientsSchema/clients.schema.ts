import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Client {
  @Field()
  id: number;

  @Field()
  name: string;
}
