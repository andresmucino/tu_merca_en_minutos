import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { AppResolver } from './app.resolver';
import { ClientsModule } from './clients/clients.module';
import { join } from 'path';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientsEntity } from './clients/entities/client.entity';

@Module({
  imports: [
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
      playground: true,
      definitions: {
        path: join(process.cwd(), 'src/schema.gql')
      }
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: '127.0.0.1',
      port: 3306,
      username: 'root',
      password: '123456',
      database: 'clients',
      entities: [ClientsEntity],
      synchronize: true
    }),
    ClientsModule
  ],
  controllers: [],
  providers: [AppResolver],
})
export class AppModule {}
